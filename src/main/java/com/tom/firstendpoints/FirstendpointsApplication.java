package com.tom.firstendpoints;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableAutoConfiguration
public class FirstendpointsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstendpointsApplication.class, args);
	}
	@RequestMapping(method = RequestMethod.POST)
	public static String firstPost() {
		return "my first POST";
	}
	@RequestMapping(method = RequestMethod.GET)
	public static String firstGet() {
		return "my first GET";
	}
	@RequestMapping(method = RequestMethod.PUT)
	public static String firstPut() {
		return "my first PUT";
	}
	@RequestMapping(method= RequestMethod.DELETE)
	public static String firstDelete() {
		return "my first DELETE";
	}
}

