package com.tom.firstendpoints;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


@RunWith(SpringRunner.class)
//@SpringBootTest
@WebMvcTest(FirstendpointsApplication.class)
//@ContextConfiguration(classes=FirstendpointsApplication.class)
public class FirstendpointsApplicationTests {

	@Autowired
	private MockMvc mvc;

	@Test
	public void testPostShouldReturnString() throws Exception {

		MvcResult mvcResult = mvc.perform(post("/").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		String result = mvcResult.getResponse().getContentAsString();
		assertEquals("my first POST", result);
	}
	
	@Test
	public void testGetShouldReturnString() throws Exception {
		MvcResult mvcResult = mvc.perform(get("/").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		String result = mvcResult.getResponse().getContentAsString();
		assertEquals("my first GET", result);
		
	}
	
	@Test
	public void testPutShouldReturnString() throws Exception {
		MvcResult mvcResult = mvc.perform(put("/").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		String result = mvcResult.getResponse().getContentAsString();
		assertEquals("my first PUT", result);
	}

	@Test
	public void testDeleteShouldReturnString() throws Exception {
		MvcResult mvcResult = mvc.perform(delete("/").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		String result = mvcResult.getResponse().getContentAsString();
		assertEquals("my first DELETE", result);
	}
}
